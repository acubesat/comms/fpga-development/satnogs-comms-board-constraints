## This file is a general .xdc for the SatNOGS-COMMS board (AcubeSAT fork)
## TO use it in a project:
## - uncomment the lines corresponding to the used pins
## - rename the used ports (in each line, after get_ports) according to the top level signal names in the project (check your block design or your HDL wrapper)
## Corresponding user guide: https://docs.xilinx.com/r/en-US/ug903-vivado-using-constraints
## To understand how to read long pin names, read this: https://docs.xilinx.com/v/u/en-US/ug865-Zynq-7000-Pkg-Pinout

## Clock signal, 100MHz according to the vendor (https://www.renesas.com/us/en/document/dst/xfn336100000000i-datasheet-addendum?r=461511)

#set_property -dict { PACKAGE_PIN Y7   IOSTANDARD LVCMOS33 } [get_ports { sysclk }]; #IO_L13P_T2_MRCC_13
## Define a clock with period of 10ns, 0 degrees and duty cycle of 50%
#create_clock -period 10.00 -waveform {0 5} [get_ports { sysclk }];

## IQ Rx Clock

#set_property -dict { PACKAGE_PIN K17   IOSTANDARD LVDS_25 } [get_ports { iq_rx_clk_p }]; #IO_L12P_T1_MRCC_35
#set_property -dict { PACKAGE_PIN K18   IOSTANDARD LVDS_25 } [get_ports { iq_rx_clk_n }]; #IO_L12N_T1_MRCC_35
#set_property DIFF_TERM TRUE [get_ports { iq_rx_clk_p }];
#set_property DIFF_TERM TRUE [get_ports { iq_rx_clk_n }];
## Define a clock with period of 15.626ns, 0 degrees phase shift and duty cycle of approx. 50%
## The value of the clock corresponds to 64MHz frequency as defined in:
## https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42415-WIRELESS-AT86RF215_Datasheet.pdf#page=22
#create_clock -period 15.625 -name iq_rx_clk_p -waveform {0.000 7.813} [get_ports iq_rx_clk_p]

## IQ Tx Clock

#set_property -dict { PACKAGE_PIN L16   IOSTANDARD LVDS_25 } [get_ports { iq_tx_clk_p }]; #IO_L11P_T1_SRCC_35
#set_property -dict { PACKAGE_PIN L17   IOSTANDARD LVDS_25 } [get_ports { iq_tx_clk_n }]; #IO_L11N_T1_SRCC_35
## TODO: Verify internal termination
#set_property DIFF_TERM TRUE [get_ports { iq_tx_clk_p }];
#set_property DIFF_TERM TRUE [get_ports { iq_tx_clk_n }];
## Define a clock with period of 15.626ns, 0 degrees phase shift and duty cycle of approx. 50%
## The value of the clock corresponds to 64MHz frequency as defined in:
## https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-42415-WIRELESS-AT86RF215_Datasheet.pdf#page=22
#create_clock -period 15.625 -name iq_rx_clk_p -waveform {0.000 7.813} [get_ports iq_tx_clk_p]

## WARNUNG!!!: The IQ Rx clock and the IQ Tx clock should be of the same frequency

## Resets, note that these may not be used

#set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33 } [get_ports { reset_ps }]; #IO_L2N_T0_34
#set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33 } [get_ports { reset_09 }]; #IO_L2P_T0_34
#set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { reset_24 }]; #IO_L6N_T0_VREF_34

## SPI

#set_property -dict { PACKAGE_PIN Y19   IOSTANDARD LVCMOS33 } [get_ports { spi_miso }]; #IO_L17N_T2_34
#set_property -dict { PACKAGE_PIN Y17   IOSTANDARD LVCMOS33 } [get_ports { spi_mosi }]; #IO_L7N_T1_34
#set_property -dict { PACKAGE_PIN P20   IOSTANDARD LVCMOS33 } [get_ports { spi_clk }]; #IO_L14N_T2_SRCC_34
#set_property -dict { PACKAGE_PIN Y16   IOSTANDARD LVCMOS33 } [get_ports { spi_sel }]; #IO_L7P_T1_34

## IRQ

#set_property -dict { PACKAGE_PIN Y11   IOSTANDARD LVCMOS33 } [get_ports { irq_pin }]; #IO_L18N_T2_13

## Rx09 IQ Core

#set_property -dict { PACKAGE_PIN J20   IOSTANDARD LVDS_25 } [get_ports { iq_rxd_09_p }]; #IO_L17P_T2_AD5P_35
#set_property -dict { PACKAGE_PIN H20   IOSTANDARD LVDS_25 } [get_ports { iq_rxd_09_n }]; #IO_L17P_T2_AD5P_35
#set_property DIFF_TERM TRUE [get_ports { iq_rxd_09_p }];
#set_property DIFF_TERM TRUE [get_ports { iq_rxd_09_n }];

## Rx24 IQ Core

#set_property -dict { PACKAGE_PIN M19   IOSTANDARD LVDS_25 } [get_ports { iq_rxd_24_p }]; #IO_L7P_T1_AD2P_35
#set_property -dict { PACKAGE_PIN M20   IOSTANDARD LVDS_25 } [get_ports { iq_rxd_24_n }]; #IO_L7N_T1_AD2N_35
#set_property DIFF_TERM TRUE [get_ports { iq_rxd_24_p }];
#set_property DIFF_TERM TRUE [get_ports { iq_rxd_24_n }];

## Tx IQ Core

#set_property -dict { PACKAGE_PIN C20   IOSTANDARD LVDS_25 } [get_ports { iq_txd_p }]; #IO_L1P_T0_AD0P_35
#set_property -dict { PACKAGE_PIN B20   IOSTANDARD LVDS_25 } [get_ports { iq_txd_n }]; #IO_L1N_T0_AD0N_35
## TODO: Verify internal termination
#set_property DIFF_TERM TRUE [get_ports { iq_txd_p }];
#set_property DIFF_TERM TRUE [get_ports { iq_txd_n }];

## Gain set pins are not constrained, since they will not be used by AcubeSAT

## LEDs

#set_property -dict { PACKAGE_PIN N17   IOSTANDARD LVCMOS33 } [get_ports { leds_2bits_tri_o[0] }]; #IO_L23P_T3_34
#set_property -dict { PACKAGE_PIN P18   IOSTANDARD LVCMOS33 } [get_ports { leds_2bits_tri_o[1] }]; #IO_L23N_T3_34
## TODO: Verify if a specific drive strength is indeed needed and if it is indeed necessary, find it's value
#set_property DRIVE 12 [get_ports { leds_2bits_tri_o[0] }];
#set_property DRIVE 12 [get_ports { leds_2bits_tri_o[1] }];
