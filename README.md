# SatNOGS COMMS Board Constraints

In this repository resides the constraint file of the [SatNOGS COMMS Board v0.A3S](https://gitlab.com/acubesat/comms/hardware/satnogs-comms-hardware).

## How to use

Clone the repository
```
git clone https://gitlab.com/acubesat/comms/fpga-development/satnogs-comms-board-constraints
```
To use it in your Vivado project, follow these steps:
- In your open project click `Add Sources` in the `Project Manager` section of the `Flow Navigator`. A dialog window will be launched.
- On the first screen, select `Add or create constraints`. Click `Next` to continue.
- In the next screen, make sure that the constraint set specified is set to `constrs_1`, and that it is the active set. Click the `Add Files` button.
- In the dialog window that pops up, navigate to the folder that you previously cloned the repo into. Select the `.xdc` file for the board, then click `OK` to continue.
- Back in the `Add Sources dialog`, make sure that the constraint file appears in the table. Also, make sure that the `Copy constraint files into project box` is checked. If this box is unchecked, the file will be linked by your project, and any modifications made within the project will affect the version you downloaded. Since you may need to use this file again in other projects, copying the constraint file is recommended, so that you can always work from a fresh copy.
- Click Finish to add the constraint file to your project.
- The constraint file should appear in the `Sources` section of the project. Use it as you wish.
